
"Powerline
let g:Powerline_symbols='fancy'
set laststatus=2
set encoding=utf-8

if $COLORTERM == 'gnome-terminal'
  set t_Co=256
endif
set nocompatible

execute pathogen#infect()
" omoguceno sejvanje sa ctrl+s u .bashrc dodano stty -ixon
nmap <c-s> :w<CR> 
imap <c-s> <Esc>:w<CR>
imap <c-s> <Esc><c-s>
nmap <c-q> :q<CR>
imap jj <ESC>
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
nmap <C-v> :vertical resize +5<cr>
nmap 50 <c-w>=
nmap <C-b> :NERDTreeToggle<cr>
highlight Search cterm=underline

set mouse=a
set linespace=15
set tabstop=4
let mapleader=","
autocmd vimenter * NERDTree  "otvara NerdTree odmah na pocetku"

autocmd VimEnter * wincmd p  "stavlja kursor na file ne na nerdtree"
let g:NERDTreeWinPos = "left"

syntax on
colorscheme xoria256

"colorscheme monoka"
set noshowmode
set number
filetype indent plugin on
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window if the only
" " buffer that's left is the NERDTree buffer
 function! s:CloseIfOnlyNerdTreeLeft()
   if exists("t:NERDTreeBufName")
       if bufwinnr(t:NERDTreeBufName) != -1
             if winnr("$") == 1
                     q
                           endif
                               endif
                                 endif
                                 endfunction


au BufRead,BufNewFile *.php set ft=php.html "dopusta uporabu html tagova u .php file-u"

"Laravel
abbrev gm !php artisan generate:model
abbrev gc !php artisan generate:controller
abbrev gmig !php artisan generate:migration


autocmd BufWritePre *.php :%s/\s\+$//e

nmap <leader>lr :vsp app/routes.php<cr>
nmap <leader>lca :vsp app/config/app.php<cr>
nmap <leader>lcd :vsp app/config/database.php<cr>
